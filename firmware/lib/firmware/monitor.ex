defmodule Firmware.Monitor do
  use GenServer
  alias Circuits.GPIO
  alias Firmware.Connection
  require Logger

  ## Client API

  @doc """
  Starts the registry.
  """
  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: Monitor)
  end

  ## Server Callbacks

  def init(:ok) do
    # Set up the pins to represent a 2 x 8 grid of wires
    left = for {pin, index} <- Enum.with_index(4..4 + 7) do
      Logger.info("Starting pin #{pin} as output")
      {:ok, output} = GPIO.open(pin, :output)

      %{pin: output, index: index}
    end

    right = for {pin, index} <- Enum.with_index(16..16 + 7) do
      Logger.info("Starting pin #{pin} as input")
      {:ok, input} = GPIO.open(pin, :input)
      GPIO.set_pull_mode(input, :pulldown)

      %{pin: input, index: index}
    end

    check_wires()

    {:ok, %{left: left, right: right, connections: %{}}}
  end

  def handle_info(:check_wires, %{left: left, right: right, connections: connections} = state) do
    check_wires()

    connections = Enum.map(left, fn %{pin: pin, index: index} ->
        pair = check_connections(pin, right)

        connection = Connection.debounce(connections, index, pair)
        Connection.try_update(connection, index)

        {index, connection}
      end)
    |> Enum.into(%{})

    {:noreply, %{state | connections: connections}}
  end

  defp check_connections(output, potentials) do
    GPIO.write(output, 1)

    pair = case Enum.find(potentials, fn %{pin: input} -> GPIO.read(input) == 1 end) do
      nil -> nil
      pair -> pair.index
    end

    GPIO.write(output, 0)

    pair
  end

  defp check_wires() do
    Process.send_after(Monitor, :check_wires, 20) # Check the wires every twenty milliseconds
  end
end
