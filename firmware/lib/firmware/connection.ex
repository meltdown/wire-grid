defmodule Firmware.Connection do
  use Bitwise, only_operators: true
  alias Firmware.Connection
  require Logger

  @enforce_keys [:pin, :state, :action]
  defstruct [:pin, :state, :action]

  def try_update(_connection, nil), do: :error
  def try_update(%{action: :wait}, _index), do: :ok
  def try_update(%{action: :unplug}, index), do: Ui.State.unpair(index)
  def try_update(%{action: :plug, pin: pin}, index) do
    Ui.State.pair(Ui.State.lookup("left", index), Ui.State.lookup("right", pin))
  end

  def debounce(connections, index, new_pin) do
    case Map.get(connections, index) do
      nil ->
        %Connection{pin: new_pin, state: 0, action: :wait}
      %{pin: pin} = connection when is_nil(pin) or pin != new_pin ->
        [state, action] = do_debounce(connection.state, 0)

        if state == 0 do
          %Connection{pin: new_pin, state: state, action: action}
        else
          %Connection{connection | state: state, action: action}
        end
      connection ->
        [state, action] = do_debounce(connection.state, 1)
        %Connection{connection | state: state, action: action}
    end
  end

  defp do_debounce(state, reading) do
    # Debouncing algorithm is from https://hackaday.com/2015/12/10/embed-with-elliot-debounce-your-noisy-buttons-part-ii
    state = ((state <<< 1) ||| (state >>> 8)) &&& 0xFF ||| reading

    cond do
      (state &&& 0xC3) == 0x03 -> [0xFF, :plug]
      (state &&& 0xC3) == 0xC0 -> [0, :unplug]
      true -> [state, :wait]
    end
  end
end
