defmodule Firmware.LEDs do
  use GenServer
  alias Nerves.Leds
  alias Ui.State.Grid
  require Logger

  ## Client API

  @doc """
  Starts the registry.
  """
  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: LEDs)
  end

  @doc """
  Tells the server to get the latest LED status (and set the LEDs as appropriate).
  """
  def refresh() do
    Process.send(LEDs, {:refresh}, [])
  end

  ## Server Callbacks

  def init(:ok) do
    Logger.debug "Setting initial LED state"

    # Start with the LED off
    Leds.set([{"led0", false}])

    # Subscribe to updates from the state genserver
    ref = Phoenix.PubSub.subscribe(Nerves.PubSub, "grid")

    {:ok, {ref}}
  end

  def handle_info({:refresh}, state) do
    Logger.info "Refreshing LEDs"

    # Look up the state and then set the LEDs as appropriate
    {:ok, grid} = Ui.State.lookup()

    if Grid.is_solved?(grid) do
      Leds.set([{"led0", true}])
    else
      Leds.set([{"led0", false}])
    end

    {:noreply, state}
  end
end

