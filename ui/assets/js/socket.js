import {Socket} from "phoenix"
import Line from './line.js'

let socket = new Socket("/socket", {params: {}})

socket.connect()

let channel = socket.channel("grid:lobby", {})

Line.initialise(channel)

channel.on("cycle", payload => {
  let elements = document.querySelectorAll(".grid > span:first-child")
  let grid = payload.grid.map(function(item, index) {
    return [item, elements[index]]
  })

  grid.forEach(function (item) {
    item[1].innerText = item[0]
  })
})

channel.join()
  .receive("ok", resp => {
    console.log("Joined successfully")
    updateGrid(JSON.parse(resp))
  })
  .receive("error", resp => { console.log("Unable to join", resp) })

function updateGrid(grid) {
  grid.forEach(function(item, index) {
    document.querySelector(`.grid:nth-child(${index + 1}) span.left .content`).innerText = item[0]
    document.querySelector(`.grid:nth-child(${index + 1}) span.right .content`).innerText = item[1]
  })
}

export default socket
