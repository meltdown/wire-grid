import LeaderLine from 'leader-line'

var Line = (function() {
    "use strict";

    var channel = null
    var leftSelected = null
    var rightSelected = null
    var lines = []

    function addLine(left, right) {
      right.dataset.selected = left.dataset.index
      left.classList.add("selected")
      right.classList.add("selected")

      lines[left.dataset.index] = new LeaderLine(
        left,
        right,
        {startPlug: 'square', endPlug: 'square', path: 'magnet'}
      )
    }

    function removeLine(index) {
      let line = lines[index]
      let left = document.querySelector(`.grid span.left input[data-index='${index}']`)
      let right = document.querySelector(`.grid span.right input[data-selected='${index}']`)

      if (left == null || right == null) {
        return
      }

      if (line != null) {
        line.remove()
        lines[index] = null
      }

      left.classList.remove("selected")
      right.classList.remove("selected")
      right.dataset.selected = null
    }

    function select(target, column, selector) {
      if (target.classList.contains("selected")) {
        removeLine((target.dataset.selected != null) ? target.dataset.selected : target.dataset.index)

        // disconnect
        channel.push("unpair_request", {index: parseInt(index)})
      }

      document.querySelectorAll(selector).forEach(function(input) {
        if (input != leftSelected && input != rightSelected) {
          input.checked = false
        }
      })

      if (leftSelected != null && rightSelected != null) {
        channel.push(
            "pair_request",
            {
                left: parseInt(leftSelected.dataset.index),
                right: parseInt(rightSelected.dataset.index)
            }
        )

        leftSelected.checked = false
        rightSelected.checked = false
        leftSelected = null
        rightSelected = null
      }
    }

    function addChannelHandlers(chan) {
      channel = chan

      chan.on("pair", payload => {
        let left = document.querySelector(`.grid span.left input[data-answer='${payload.left}']`)
        let right = document.querySelector(`.grid span.right input[data-answer='${payload.right}']`)

        addLine(left, right)

        document.querySelector('.grid-solved').dataset.solved = payload.solved
      })

      chan.on("unpair", payload => {
        removeLine(payload.index)
        document.querySelector('.grid-solved').dataset.solved = false
      })
    }

    function setupLines() {
      let group = document.querySelector(".group")
      let connections = JSON.parse(group.dataset.connections)
      Object.keys(connections).forEach(function (index) {
        let left = document.querySelector(`.grid span.left input[data-index='${index}']`)
        let right = document.querySelector(`.grid span.right input[data-answer='${connections[index].index}']`)

        addLine(left, right)
      })
    }

    function addLineHandlers() {
      let leftInputs = document.querySelectorAll(".grid span.left input")
      let rightInputs = document.querySelectorAll(".grid span.right input")

      leftInputs.forEach(function(input) {
        lines.push(null)
        input.addEventListener("click", event => {
          leftSelected = event.target
          select(event.target, "left", ".grid span.left input")
        })
      })

      rightInputs.forEach(function(input) {
        input.addEventListener("click", event => {
          rightSelected = event.target
          select(event.target, "right", ".grid span.right input")
        })
      })
    }

    function initialise(channel) {
      addChannelHandlers(channel)
      setupLines()
      addLineHandlers()
    }

    return {
      initialise: initialise,
      addLine: addLine
    };
}())

export default Line
