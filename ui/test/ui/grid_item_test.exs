defmodule Ui.State.GridItemTest do
  use ExUnit.Case
  alias Ui.State.GridItem

  test "to string" do
    item = %GridItem{data: 20, width: 5}

    assert "░ ▓ ░ ▓ ▓ " == "#{item}"
  end
end
