defmodule Ui.State.GridTest do
  use ExUnit.Case
  alias Ui.State.GridItem
  alias Ui.State.Grid

  test "single row grid from seed" do
    seed = "testseed"
    grid = Grid.list_from_seed(seed, 4, 1)

    assert build_grid([{7, 0}], [{4, 0}], 4) == grid
  end

  test "randomised with multiple rows" do
    seed = "testseed"
    grid = Grid.list_from_seed(seed, 5, 3)

    assert build_grid([{14, 0}, {17, 1}, {18, 2}], [{6, 1}, {23, 0}, {29, 2}], 5) == grid
  end

  test "pairing" do
    seed = "testseed"
    grid = Grid.list_from_seed(seed, 5, 3)

    left = Enum.at(grid.left, 1)
    right = Enum.at(grid.right, 1)

    grid = Grid.pair(grid, left, right)

    comparator =
      build_grid([{14, 0}, {17, 1}, {18, 2}], [{6, 1}, {23, 0}, {29, 2}], 5)
      |> Map.put(:connections, %{1 => right})

    assert comparator == grid
  end

  test "unpairing" do
    seed = "testseed"
    grid = Grid.list_from_seed(seed, 5, 3)

    left = Enum.at(grid.left, 1)
    right = Enum.at(grid.right, 1)

    left2 = Enum.at(grid.left, 2)
    right2 = Enum.at(grid.right, 0)

    grid =
      grid
      |> Grid.pair(left, right)
      |> Grid.pair(left2, right2)

    left_unpaired = Grid.unpair(grid, :left, left.index)
    right_unpaired = Grid.unpair(grid, :right, right)

    comparator =
      build_grid([{14, 0}, {17, 1}, {18, 2}], [{6, 1}, {23, 0}, {29, 2}], 5)
      |> Map.put(:connections, %{2 => right2})

    assert left_unpaired == comparator
    assert right_unpaired == comparator
  end

  test "combining" do
    seed = "testseed"
    grid = Grid.list_from_seed(seed, 5, 3)

    comparator = [
      %GridItem{data: 6, index: nil, width: 5},
      %GridItem{data: 0, index: nil, width: 5},
      %GridItem{data: 16, index: nil, width: 5}
    ]

    assert Grid.combined(grid) == comparator
  end

  test "checking if solved" do
    seed = "testseed"
    grid = Grid.list_from_seed(seed, 5, 3)

    refute Grid.is_solved?(grid)

    grid =
      grid
      |> Grid.pair(Enum.at(grid.left, 0), Enum.at(grid.right, 1))
      |> Grid.pair(Enum.at(grid.left, 1), Enum.at(grid.right, 0))

    refute Grid.is_solved?(grid)

    grid = Grid.pair(grid, Enum.at(grid.left, 2), Enum.at(grid.right, 2))

    assert Grid.is_solved?(grid)
  end

  defp build_grid(left, right, width) do
    %Grid{
      left: list_to_grid_items(left, width),
      right: list_to_grid_items(right, width),
      connections: %{},
      width: width
    }
  end

  defp list_to_grid_items(list, width) do
    Enum.map(list, fn {data, index} -> %GridItem{data: data, index: index, width: width} end)
  end
end
