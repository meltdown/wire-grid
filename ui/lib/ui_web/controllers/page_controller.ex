defmodule UiWeb.PageController do
  use UiWeb, :controller

  def index(conn, _params) do
    {:ok, grid} = Ui.State.lookup()
    render conn, "index.html", grid: grid
  end
end
