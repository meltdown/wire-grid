defmodule UiWeb.GridChannel do
  use Phoenix.Channel

  def join("grid:lobby", _message, socket) do
    {:ok, grid} = Ui.State.lookup()

    grid = Stream.zip(grid.left, grid.right)
    |> Enum.map(fn {left, right} ->
      ["#{left}", "#{right}"]
    end)

    {:ok, Jason.encode!(grid), socket}
  end

  def join("grid:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_in("unpair_request", %{"index" => index}, socket) do
    Ui.State.unpair(index)
    {:noreply, socket}
  end

  def handle_in("pair_request", %{"left" => left, "right" => right}, socket) do
    left = Ui.State.lookup("left", left)
    right = Ui.State.lookup("right", right)

    if (left && right) do
      Ui.State.pair(left, right)
    end

    {:noreply, socket}
  end
end
