defmodule Ui.State do
  use GenServer
  alias Ui.State.Grid
  alias Ui.State.GridItem
  require Logger

  ## Client API

  @doc """
  Starts the registry.
  """
  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: State)
  end

  @doc """
  Gets the whole grid

  Returns `{:ok, grid}`, `:error` otherwise.
  """
  def lookup() do
    GenServer.call(State, {:lookup})
  end

  @doc """
  Gets an item from the grid

  Returns `grid_item` if the item is found, or nil otherwise
  """
  def lookup(column, index) do
    GenServer.call(State, {:lookup, column, index})
  end

  @doc """
  Connects two sides of the grid.

  Override is used to make the web UI override the physical connections from the firmware. Even if it is set it is only
  applied when necessary.

  Returns `{:ok, grid}`, `:error` otherwise.
  """
  def pair(left, right, override \\ false) do
    GenServer.call(State, {:pair, left, right, override})
  end

  @doc """
  Disconnects a pair

  Override is used to make the web UI override the physical connections from the firmware. Even if it is set it is only
  applied when necessary.
  """
  def unpair(index, override \\ false) do
    GenServer.cast(State, {:unpair, index, override})
  end

  ## Server Callbacks

  def init(:ok) do
    Logger.debug "Initialising state"
    seed = Enum.take_random('abcdefghijklmnopqrstuvwxyz1234567890', 24)
    schedule_cycle()
    {:ok, %{grid: Grid.list_from_seed("#{seed}", 6, 7)}}
  end

  def handle_call({:lookup}, _from, %{grid: grid} = state) do
    {:reply, {:ok, grid}, state}
  end

  def handle_call({:lookup, "left", idx}, _from, %{grid: grid} = state), do: {:reply, Enum.at(grid.left, idx), state}
  def handle_call({:lookup, "right", idx}, _from, %{grid: grid} = state), do: {:reply, Enum.at(grid.right, idx), state}

  def handle_call({:pair, left, right, override}, _from, %{grid: grid} = state) do
    grid =
      grid
      |> Grid.unpair(:left, left.index)
      |> Grid.unpair(:right, right)
      |> Grid.pair(left, right)

    UiWeb.Endpoint.broadcast!("grid:lobby", "pair", %{
      left: left.index,
      right: right.index,
      solved: Grid.is_solved?(grid)
    })

    {:reply, {:ok, grid}, %{state | grid: grid}}
  end

  def handle_cast({:unpair, index, override}, %{grid: grid} = state) do
    grid = Grid.unpair(grid, :left, index)

    UiWeb.Endpoint.broadcast!("grid:lobby", "unpair", %{index: index})

    {:noreply, %{state | grid: grid}}
  end

  def handle_info(:cycle, %{grid: grid} = state) do
    schedule_cycle()

    cycled_grid =
      grid
      |> Grid.combined()
      |> GridItem.cycle()
      |> Enum.map(& "#{&1}")

    UiWeb.Endpoint.broadcast!("grid:lobby", "cycle", %{grid: cycled_grid})

    {:noreply, state}
  end

  defp schedule_cycle() do
    Process.send_after(self(), :cycle, 500)
  end
end
