defmodule Ui.State.Grid do
  alias Ui.State.Grid
  alias Ui.State.GridItem
  use Bitwise, only_operators: true

  @derive Jason.Encoder
  @enforce_keys [:left, :right, :connections, :width]
  defstruct [:left, :right, :connections, :width]

  def list_from_seed(seed, width, rows) do
    seed_list = (for <<x :: size(width) <- seed >>, do: x)

    [left, right] =
      # Convert the seed into an array of nibbles
      seed_list
      # Take enough numbers to build the grid
      |> Stream.take(rows * 2)
      # Turn it into a list of grid items
      |> Stream.map(& %GridItem{data: &1, width: width})
	  |> Stream.chunk_every(rows)
      # Add an index so we can track which grid items belong to each other
      |> Enum.map(fn column ->
		Stream.with_index(column)
		# Actually add the index
		|> Enum.map(fn {grid_item, index} -> %GridItem{grid_item | index: index} end)
	  end)

    # Seed the random number generator so we always get the same output
	[a, b, c | _] = seed_list
    :rand.seed(:exsplus, {a, b, c})

	# Mix up the columns and build the grid
    %Grid{left: left, right: mix_up(right), connections: %{}, width: width}
  end

  def is_solved?(grid) do
    combined(grid) === connected(grid)
  end

  def combined(grid) do
    Enum.zip(grid.left, Enum.sort_by(grid.right, & &1.index))
    |> Stream.map(fn {left, right} -> left.data &&& right.data end)
    |> Enum.map(& %GridItem{data: &1, width: grid.width})
  end

  def connected(grid) do
    grid.left
    |> Stream.map(fn left ->
        case Map.get(grid.connections, left.index) do
          nil -> nil
          right -> left.data &&& right.data
        end
      end)
    |> Enum.map(& %GridItem{data: &1, width: grid.width})
  end

  def unpair(grid, :left, index) do
    %Grid{grid | connections: Map.delete(grid.connections, index)}
  end

  def unpair(grid, :right, item) do
    case Enum.find(grid.connections, fn {_left, right} -> right == item end) do
      nil -> grid
      {index, _} -> unpair(grid, :left, index)
    end
  end

  def pair(grid, left, right) do
    %Grid{grid | connections: Map.put(grid.connections, left.index, right)}
  end

  defp mix_up([column]), do: [column]
  defp mix_up(column) do
	shuffled = Enum.shuffle(column)

	if shuffled == column do
	  mix_up(column)
	else
	  shuffled
	end
  end
end
