defmodule Ui.State.GridItem do
  alias Ui.State.GridItem

  @derive Jason.Encoder
  @enforce_keys [:data, :width]
  defstruct [:data, :index, :width]

  def cycle(grid) do
    size = Enum.count(grid)
    off = Enum.map(0..div(size, 3), fn _ -> :rand.uniform(size) end)

    grid
    |> Stream.with_index
    |> Enum.map(fn {item, index} ->
      %GridItem{data: (if Enum.member?(off, index), do: 0, else: item.data), width: item.width}
    end)
  end

  defimpl String.Chars, for: GridItem do
    def to_string(item) do
      # Take each bit from the data
      width = item.width
      (for <<x :: 1 <- <<item.data :: size(width)>> >>, do: x)
      # Create a graphical representation of the bits
      |> Enum.reduce("", fn bit, str ->
        str <> case bit do
          0 -> "▓ "
          1 -> "░ "
        end
      end)
    end
  end
end
